# FRONTEND-STARTER-KIT
## SETUP YOUR FRONT-END PROJECT
### PREREQUISITE
* NODE Environment( To Install go [here](https://nodejs.org) )
* GIT ( To Install go [here](https://git-scm.com) )
* BASIC COMMAND LINE KNOWLEDGE

### LETS START ( Up and Running )

Step 1: Create your project directory and clone frontend-starter-kit into it.
    
    $ mkdir your-project-name && cd your-project-name
    $ git clone https://gitlab.com/neamat/frontend-starter-kit.git .
    
Step 2: Remove .git folder which is specific for frontent-starter-kit and initiate and then configure your own git.
    
    $ rm -fR .git && git init
    
Step 3: Install all dev dependancies

    $ npm install

Step 5: Follow frontend-strater-kit folder structure for your project resources.

Step 6: Compile your resources for development run

    $ npm run dev
    
Step 6: Compile your resources for development build run

    $ npm run build
    
Step 6: Compile your resources for production run

    $ npm run prod
    
## DEVELOPMENT CONVENTION OR GUIDELINE
### SYNTAX SPECIFIC SETUP ( [CODEGUIDE](https://codeguide.co) )
HTML
* Use soft tabs with two spaces
* 80 character wide columns
* Nested elements should be indented once (two spaces).
* Don’t omit optional closing tags
* Whenever possible reduce the markup

        <!-- Not so great -->
        <span class="avatar">
        <img src="...">
        </span>

        <!-- Better -->
        <img class="avatar" src="...">
* Avoid JavaScript generated markup whenever possible.

ATTRIBUTE
* Always use double quotes
* Don't add a value to Boolean attributes
* HTML attribute order

        class
        id, name
        data-*
        src, for, type, href, value
        title, alt
        role, aria-*

CSS
* Use soft tabs with two spaces
* Declaring the @charset directive on top of the stylesheet is highly recommended.
* When grouping selectors, keep individual selectors to a single line. 
* Include one space before the opening brace of declaration blocks.
* Place closing braces of declaration blocks on a new line.
* Place a new line after the closing brace }
* Include one space after : for each declaration.
* Each declaration should appear on its own line.
* Comma-separated property values should include a space after each comma (e.g., box-shadow).
* Don't include spaces after commas within rgb(), rgba(), hsl(), hsla(), or rect() values.
* Don't prefix property values or color parameters with a leading zero
* Lowercase all hex values
* Use shorthand hex values where available
* Quote attribute values in selectors
* Avoid specifying units for zero values
* Properties declaration order (USE TYPE ORDERING)
    
        Positioning
        Box model
        Typographic
        Visual
* Strive to limit use of shorthand declarations to instances where you must explicitly set all the available values.
* Scope classes to the closest parent only when necessary 

### ORGANIZING 
* Begin every new major section of a project with a title

        /*------------------------------------*\
         #SECTION-TITLE
        \*------------------------------------*/
* Meaningful Whitespace
    * One (1) empty line between loosely related rulesets.
    * Two (2) empty lines between between entirely new sections.
    

### COMMENTING
CSS
* CSS needs more comments.

    As CSS is something of a declarative language that doesn’t really leave much of a paper-trail, it is often hard to discern—from looking at the CSS alone—

    * whether some CSS relies on other code elsewhere;
    * what effect changing some code will have elsewhere;
    * where else some CSS might be used;
    * what styles something might inherit (intentionally or otherwise);
    * what styles something might pass on (intentionally or otherwise);
    * where the author intended a piece of CSS to be used.

### NAMING CONVENTION BEM ( [BEM](https://getbem.com) )
A good naming convention will tell you and your team
* what type of thing a class does;
* where a class can be used;
* what (else) a class might be related to.

BEM

BEM splits components’ classes into three groups:

* Block: The sole root of the component.
* Element: A component part of the Block.
* Modifier: A variant or extension of the Block.

        .person { }
        .person__head { }
        .person--tall { }
* Elements are delimited with two (2) underscores (__), and Modifiers are delimited by two (2) hyphens (--).
* HTML naming

        <div class="box  profile  profile--is-pro-user">
            <img class="avatar  profile__image" />
            <p class="profile__bio">...</p>
        </div>
JAVASCRIPT HOOKS

As a rule, it is unwise to bind your CSS and your JS onto the same class in your HTML. This is because doing so means you can’t have (or remove) one without (removing) the other. It is much cleaner, much more transparent, and much more maintainable to bind your JS onto specific classes.
*  javaScript classes prepended with js-

        <input type="submit" class="btn  js-btn" value="Follow">
* data-* attributes are designed to store data, not be bound to. so use it correctly.

## Sass or SCSS
* Use Native CSS variable rather than Sass/SCSS variable.
* Please Follow this sass-guidelin.es/#too-long-didnt-read link for Sass or SCSS related guidelines.

### NOTE
* Follow The Single Responsibility Principle for your class.
* Follow The Open/Closed Principle for your code
* Follow DRY (The key isn’t to avoid all repetition, but to normalise and abstract meaningful repetition)
* Follow Composition over Inheritance
* Follow The Separation of Concerns (the concepts of modularity and encapsulation in code. In CSS this means building individual components, and writing code which only focusses itself on one task at a time.)


### WARNING
* Keep in mind that sometimes KISS (Keep It Simple, Stupid) is better than DRY (Don’t Repeat Yourself)


### FINAL TOUCH AND DEPLOYMENT

* Create favicon using realfavicongenerator.net
* Performance optimization for site speed use our webpack production build before deploy
    
        npm run prod

* Basic search engine optimization (SEO)
    * Update meta description tag
    * Validate HTML and CSS
    * Content is king (Keyword: Use Keyword in your meta description, title. headings and links)
    * Use backlinks
* Google Analytics
    * Use google analytics to track your user for further improvement of your website.

If you want to khow how this project was created then read [DETAILS](https://gitlab.com/neamat/frontend-starter-kit/blob/master/DETAILS.md) page.


-----
Heavily inspired by [cssguidelin](https://cssguidelin.es), [sass-guidelin](https://sass-guidelin.es), [codeguide](https://codeguide.co) and [bem](https://getbem.com) .


-----

 Open sourced. Copyright 2018 [@neamat](https://gitlab.com/neamat) 