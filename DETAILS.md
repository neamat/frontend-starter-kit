# How this project was created
## PREREQUISITE
  * NODE
  * ZSH ( if you use bash then replace zsh specific commond )
  * GIT


STEP 1: 
    
    $ md frontend-starter-kit && frontend-starter-kit

STEP 2: 

    $ g init && gpsup https://gitlab.example.com/neamat/frontend-starter-kit.git

STEP 3: 
    
    $ gra origin https://gitlab.example.com/neamat/frontend-starter-kit.git

STEP 4: 

    $ touch README.MD .gitignore

STEP 5: Configure .gitignore file for your project

STEP 6: 

    $ md src/resources/sass src/resources/js src/resources/img src/resources/fonts src/resources/template src/vendors/js src/vendors/fonts

STEP 7: 

    $ npm init -y

STEP 8: Configure webpack details here

STEP 9: Edit package.json to add npm script for running webpack from npm.

    - "main": "",
    + "private": true,
      "scripts": {
       + "dev": "webpack-dev-server --config webpack.dev.js",
       + "build": "webpack --config webpack.dev.js",
       + "prod": "webpack --config webpack.prod.js"
      },

STEP 10: 
    
    $ npm i -D webpack webpack-cli webpack-dev-server webpack-merge babel-loader babel-core babel-preset-env sass-loader node-sass css-loader style-loader postcss-loader autoprefixer precss mini-css-extract-plugin html-loader html-webpack-plugin file-loader image-webpack-loader svg-url-loader url-loader responsive-loader sharp purgecss-webpack-plugin glob clean-webpack-plugin

STEP 11: 

    $ src/resources/sass && md abstracts vendors base layout components pages themes

STEP 12: 
    
    $ touch main.scss

STEP 13: 

    $ abstracts && touch _variables.scss _functions.scss _mixins.scss _placeholders.scss

STEP 14: 

    $ .. && vendors && touch _bootstrap.scss _jquery-ui.scss

STEP 15: 

    $ .. && base && touch _base.scss _typography.scss

STEP 16: 

    $ .. && layout && touch _navigation.scss _grid.scss _header.scss _footer.scss _sidebar.scss _forms.scss

STEP 17: 

    $ .. && components && touch _buttons.scss _carousel.scss _cover.scss _dropdown.scss

STEP 18: 

    $ .. && pages && touch _home.scss _contact.scss

STEP 19: 

    $ .. && themes && touch _theme.scss _admin.scss

STEP 20: Afer All Those Setup Config Your script.js, main.scss, index.html and Others Project Specific Optimizations.


-----


Open sourced. Copyright 2018 [@neamat](https://gitlab.com/neamat)