/*****
 * FEATURES FOR WEBPACK.PROD.JS CONFIGURATION
 * 
 * TODOS
 * 1. Configure MiniCssExtractPlugin for Output independent or seperate CSS File
 * 2. Configure PurgecssPlugin for output purge css
 * 3. Configure responsive-loader for resizing Images
*/


const path = require('path');
const glob = require('glob');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const PurgecssPlugin = require('purgecss-webpack-plugin');


const wpMerge = require('webpack-merge');
const wpCommon = require('./webpack.comm.js');
    

const ProductionConfig = wpMerge(wpCommon, {
    mode: 'development',


    optimization: {
        nodeEnv: 'production',
        minimize: true,
        concatenateModules: true
    },


    module: {
        rules: [
            {
                test: /\.s?[ac]ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        // translates CSS into CommonJS/ES6 modules
                        loader: 'css-loader',
                        options: { 
                            minimize: true 
                        }
                    }, 
                    {
                        // Run post css actions Like Autoprefixer
                        loader: 'postcss-loader',
                        options: {
                            plugins: function () {
                                return [
                                    require('precss'),
                                    require('autoprefixer')
                                ];
                            }
                        }
                    },
                    {
                        loader: 'sass-loader' // compiles Sass to CSS
                    }
                ],
                exclude: /node_modules/
            },
            {
                //Used for image resizing
                test: /\.(jpe?g|png)$/i, 
                include: [path.resolve(__dirname, 'src', 'resources')],
                use: [
                    {
                        loader: 'responsive-loader',
                        options: {
                            adapter: require('responsive-loader/sharp'),
                            // Given Default Sizes and also work with srcset and @media quiries Sizes
                            sizes: [300, 600, 1200, 2000],
                            name: 'resources/img/[name].[hash].[ext]',
                            placeholder: true,
                            placeholderSize: 50
                        }
                    }
                ]
                
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            minimize: true,
                            removeComments: true,
                            collapseWhitespace: true
                        }
                    }
                ]
                
            }
        ]

    },


    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "resources/css/[name].[hash].css",
          //   chunkFilename: "[id].css"
        }),
        new PurgecssPlugin({
            paths: glob.sync(path.join(__dirname, 'src/resources/template/*.html'))
        })
    ]
});


module.exports = ProductionConfig;