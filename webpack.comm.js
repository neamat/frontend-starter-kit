/***
 * FEATURES FOR WEBPACK.COMMOM.JS CONFIGURATION
 * 
 * TODOS
 * 1. Configure Babel for ES6, ES7 & ES8 Compilation
 * 2. Configure for Images and fonts Optimization
 * 3. Configure HtmlWebpackPlugin for Automatic Link Insertion into our HTML File
 * 4. Configure CleanWebpackPlugin for Cleaning Our Dist Directory 
*/


// require webpack to use build-in plugins
const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');


/********************************************************************************
 * Our function that generates our multipage html plugins
 * NOTE: While in watch mode files added to the directory will not be picked up,
 * but changes to template files included this way will be.
*********************************************************************************/
const fs = require('fs');

function generateMultiPageHtmlPlugins (templateDir) {
    // Read files in template directory
    const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir))
    return templateFiles.map(item => {
      // Split names and extension
      const parts = item.split('.')
      const name = parts[0]
      const extension = parts[1]
      // Create new HTMLWebpackPlugin with options
      return new HtmlWebpackPlugin({
        filename: `${name}.html`,
        template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`)
      })
    })
}
  
// Call our function on our views directory.
const htmlPlugins = generateMultiPageHtmlPlugins('./src/resources/template');

/***********************************************************************************
 * End generate multipage HTML function
************************************************************************************/



const VENDOR_LIBS = [
  // npm vendor packages name goes here
    'jquery'
];

const CommonsConfig = {


    // context: path.resolve(__dirname, 'src'),
    entry: {
        bundle: [
            './src/resources/js/script.js',
            './src/resources/sass/main.scss',
        ],
        vendor: VENDOR_LIBS
    },
    output: {
        // Chunk Hash is used for long term cache mapping
        filename: 'resources/js/[name].[chunkhash].js',  
        path: path.resolve(__dirname, 'dist'),
        // publicPath: '/'
    },


    module: {
        rules: [
            {
                // Configuration for ES6, ES7 & ES8 Compilation using babel
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        // To Use This options .babelrc file is required
                        presets: ['env']
                    }
                },
                exclude: /node_modules/
            },
            {
                // Configuration for Images and fonts Optimization
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'resources/fonts/'
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // Inline files smaller than 10 kB (10240 bytes)
                            limit: 10 * 1024,
                            name: 'resources/img/[name].[hash].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'svg-url-loader',
                        options: {
                            // Inline files smaller than 10 kB (10240 bytes)
                            limit: 10 * 1024,
                            // Remove the quotes from the url
                            // (they’re unnecessary in most cases)
                            noquotes: true,
                            //   name: 'images/[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                // include: [path.resolve(__dirname, 'src', 'resources')],
                use: 'image-webpack-loader',
                // This will apply the loader before the other ones
                enforce: 'pre',
            }   
        
        ]
    },


    plugins: [
        new CleanWebpackPlugin(['dist'])
    ].concat(htmlPlugins)
};


module.exports = CommonsConfig;