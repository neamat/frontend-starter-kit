/*******************
 * FEATURES FOR WEBPACK.DEV.JS CONFIGURATION
 * 
 * TODOS
 * 1. Configure CSS and Sass for Style Sheet Compilation
 * 2. Configure HTML for HTML Document Compilation
 * 3. Configure Development Server for local development
*/

const wpMerge = require('webpack-merge');
const wpCommon = require('./webpack.comm.js');


const DevelopmentConfig = wpMerge(wpCommon, {
    mode: 'development',

    module: {
        rules: [
            {
                // Configuration for Style Sheet Compilation using css-loader and sass-loader 
                test: /\.s?[ac]ss$/,
                use: [
                    {
                        // inject CSS to page using <style> tag.
                        loader: 'style-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {   
                        // translates CSS into CommonJS/ES6 modules
                        loader: 'css-loader',
                        options: { 
                            sourceMap: true
                        }
                    }, 
                    {
                        // compiles Sass to CSS
                        loader: 'sass-loader',
                        options: { 
                            sourceMap: true
                        }
                    }
                ],
                // include: [path.resolve(__dirname, 'src', 'resources', 'sass')],
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: {
                            minimize: false,
                            removeComments: false,
                            collapseWhitespace: false
                        }
                    }
                ]
                
            }
        ]
    },


    devServer: {
        // static files served from here
        // contentBase: path.resolve(__dirname, "./dist/assets/media"),
        // Display only errors to reduce the amount of output.
        stats: "errors-only",

        // Parse host and port from env to allow customization.
        //
        // If you use Docker, Vagrant or Cloud9, set
        // host: options.host || "0.0.0.0";
        //
        // 0.0.0.0 is available to all network devices
        // unlike default `localhost`.
        host: process.env.HOST, // Defaults to `localhost`
        port: process.env.PORT, // Defaults to 8080
        open: true, // Open the page in browser
    }
});


module.exports = DevelopmentConfig;